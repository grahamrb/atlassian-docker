# Bamboo Docker image

This folder contains the Dockerfile and associated files for the ```grahamrb/atlassian-bamboo``` docker image based on the durdn atlassian suite of Docker images (specifically copied from the jira Dockerfile).

## Database connection

The connection to the database can be specified with an URL of the format:
```
[database type]://[username]:[password]@[host]:[port]/[database name]
```
Where ```database type``` is either ```mysql``` or ```postgresql``` and the full URL might look like this:
```
postgresql://bamboo:jellyfish@172.17.0.2/bamboodb
```

When a database url is specified Bamboo will skip the database configuration step in the setup.

## Configuration

Configuration options are set by setting environment variables when running the image. What follows it a table of the supported variables:

#### CONTEXT_PATH

Context path of the bamboo webapp. You can set this to add a path prefix to the url used to access the webapp. i.e. setting this to ```bamboo``` will change the url to http://localhost:8085/bamboo/. The value ```ROOT``` is reserved to mean that you don't want a context path prefix. Defaults to ```ROOT```

#### DATABASE_URL

Connection URL specifying where and how to connect to a database dedicated to bamboo. This variable is optional and if specified will cause the Bamboo setup wizard to skip the database setup set.

## Exposed Ports

port ```8085``` is exposed as the default port the Bamboo web ui listens on.

port ```54663``` is also exposed, this port is used by Bamboo remote agents to communicate with the master. Atlassian documents call this the bamboo broker port.

## Recommended Container Creation Commands

It is recommended to start this image with the base postgres image running along side to store the data. Below are some comments that would start 4 containers to provide the bamboo container, postgres container and a data container for each. In the example commands below the ```ubuntu``` image is used for data containers, feel free to use your preferre data container base image.

#### Postgres Data Container

```
sudo docker run -d -v /dbdata --name bamboo-postgres-data ubuntu echo Data-only container for postgres
```

#### Postgres Database Container

You may want to replace the POSTGRES_USER and POSTGRES_PASSWORD fields with something different, this isn't mandatory as the postgre container is only exposed to the Bamboo container and so is fairly secure.

```
sudo docker run --name bamboo-postgres \
--volumes-from bamboo-postgres-data \
--env PGDATA=/dbdata \
--env POSTGRES_USER=somebamboouser \
--env POSTGRES_PASSWORD=somebamboopassword \
-d postgres
```

#### Bamboo Data Container

```
sudo docker run -d -v /opt/atlassian-home --name bamboo-data ubuntu echo Data-only container for bamboo
```

#### Bamboo Application Container

Relies on Docker to add an entry to the hosts file when linking the container (which in the case is ```postgres```). Make sure you update the username and password in the command below to match those set when creating the postgres container.

```
sudo docker run -d --name bamboo \
--volumes-from bamboo-data \
--env DATABASE_URL=postgresql://somebamboouser:somebamboopassword@postgres/postgres \
--link bamboo-postgres:postgres \
-p 8085:8085 grahamrb/atlassian-bamboo
```
